## How to start

**Note** that this seed project requires node v4.x.x or higher and npm 2.14.7.

You must have `ts-node` installed as global. If you don't, use:

```bash
npm install -g ts-node
```

## Here's what these scripts do:

- run the compiler and a server at the same time, both in "watch mode"
npm start
- run the TypeScript compiler once
npm run tsc
- run the TypeScript compiler in watch mode; the process keeps running, awaiting changes to TypeScript files and re-compiling when it sees them.
npm run tsc:w
- run the lite-server, a light-weight, static file server, written and maintained by John Papa with excellent support for Angular apps that use routing.
npm run lite
- runs the typings tool
npm run typings
- called by npm automatically after it successfully completes package installation. This script installs the TypeScript definition files this app requires.
npm postinstall

## Quick start

Clone this repo
`npm i` and `npm start` and you are ready!

## [DEPLOY]

***

## Clone the repo

```sh
$ git clone https://huyd@bitbucket.org/bss-work/bss-insight.git
$ cd bss-insight
$ npm install
$ npm start
```

Good luck with angular2 hacking!
