export class Tile {
  id:number;
  name:string;
  summary:string;
  col:number;
}