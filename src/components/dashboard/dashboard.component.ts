// Angular2 specified stuff
import {Component, OnInit} from 'angular2/core';
import {Router} from 'angular2/router';

// 3rd party libraries
import {NgGrid, NgGridItem} from 'angular2-grid/src/NgGrid';

// Component specified stuff
import {LoggedIn} from '../../app/loggedIn';
import {Security} from '../../common/security/security';

import {EditItem} from './edit-item';
import {Tile} from './tile';
import {TilesService} from './tiles.service';
import {TileCardComponent} from './tile-card.component';
import {TileEditorComponent} from './tile-editor.component';

// Component setup
@Component({
  selector: 'my-dashboard',
  templateUrl: 'src/components/dashboard/dashboard.component.html',
  styleUrls: ['src/components/dashboard/dashboard.component.css'],
  directives: [
    TileCardComponent, TileEditorComponent,
    NgGrid, NgGridItem
  ],
  providers: [TilesService]
})

export class DashboardComponent extends LoggedIn implements OnInit {
  tiles:Array<EditItem<Tile>>;

  gridConfig = {
    'margins': [5],
    'draggable': true,
    'resizable': true,
    'max_cols': 3,
    'max_rows': 0,
    'min_cols': 1,
    'min_rows': 1,
    'col_width': 250,
    'row_height': 200,
    'cascade': 'up',
    'min_width': 100,
    'min_height': 100,
    'fix_to_grid': false,
    'auto_style': true,
    'auto_resize': true
  };

  constructor(public _router:Router,
              public _security:Security,
              tilesService:TilesService) {
    super(_router, _security);

    console.log(this._isAuthenticated);

    this.tiles = tilesService.getTiles()
      .map(item => new EditItem(item));
  }

  ngOnInit() {
    console.log('DashboardComponent');
  }

  onSaved(editItem:EditItem<Tile>, updatedTile:Tile) {
    editItem.item = updatedTile;
    editItem.editing = false;
  }

  onCanceled(editItem:EditItem<Tile>) {
    editItem.editing = false;

    console.log(' DashboardComponent onCanceled')
  }

  onResize(index:number, pos:{ left: number, top: number }) {
    console.log(index);
  }

  onDrag(index:number, pos:{ left: number, top: number }) {
    //console.log(index);
  }

  addTile() {

  }

  removeTile(i) {
    console.log(i);
  }

  updateItem(index:number, pos:{ col: number, row: number }) {
    //console.log(index, pos);
  }
}
