import {Tile} from './tile';

export class TilesService {
  tiles:Array<Tile> = [
    {id: 1, name: "RubberMan", summary: 'flexibility', col: 1},
    {id: 2, name: "Tornado", summary: 'Weather changer', col: 2},
    {id: 3, name: "Test 1", summary: 'Test 1', col: 3},
    {id: 4, name: "Test 2", summary: 'Test 2', col: 1}
  ];

  getTiles() {
    return this.tiles;
  }
}
