// Angular2 specified stuff
import {Component, Input} from 'angular2/core';

// Component specified stuff
import {Tile} from './tile';

// Component setup
@Component({
  selector: 'tile-card',
  template: `
    <div>
        <div>#{{tile.id}}</div>
        <div><span>Name:</span> <span>{{tile.name}}</span></div>
        <div>{{tile.summary}}</div>
    </div>`
})
export class TileCardComponent {
  @Input() tile:Tile;
}
