// Angular2 specified stuff
import {Component, Input, Output, EventEmitter} from 'angular2/core';

// Component specified stuff
import {RestoreService} from '../../common/services/services';
import {Tile} from './tile';

// Component setup
@Component({
  selector: 'tile-editor',
  providers: [RestoreService],
  template: `
    <div>
      <span>Name:</span>
      <input [(ngModel)]="tile.name"/>
      <div>
        <button (click)="onSaved()">save</button>
        <button (click)="onCanceled()">cancel</button>
      </div>
    </div>`
})

export class TileEditorComponent {
  @Output() canceled = new EventEmitter();
  @Output() saved = new EventEmitter();

  constructor(private restoreService:RestoreService<Tile>) {
  }

  @Input()
  set tile(tile:Tile) {
    this.restoreService.setItem(tile);
  }

  get tile() {
    return this.restoreService.getItem();
  }

  onSaved() {
    this.saved.next(this.restoreService.getItem());
  }

  onCanceled() {
    this.tile = this.restoreService.restoreItem();
    this.canceled.next(this.tile);

    console.log(' TileEditorComponent onCanceled')
  }
}
