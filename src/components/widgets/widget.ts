import {Component, OnInit, OnChanges, ElementRef, DynamicComponentLoader, Input, HostListener, SimpleChange} from 'angular2/core';
import {CORE_DIRECTIVES} from 'angular2/common';
import {count} from "rxjs/operator/count";

// forward declarations to avoid compiler warnings
declare var System;

class WidgetLoader {
  loadComponent(widgetContainer) {
    //console.log('****configObject: ' + JSON.stringify(widgetContainer));
    return System.import(widgetContainer.path).then(
      component => component[widgetContainer.component]
    );
  }
}

@Component({
  // the HTML tag for this component
  selector: 'my-widget',
  directives: [CORE_DIRECTIVES],
  template: `<div #content>Loading widget...</div>`,
  // bind to the widget loader
  bindings: [WidgetLoader],
  properties: ['listWidget: list-widget'],
})

export class Widget implements OnInit, OnChanges {
  listWidget:string; // Json Object
  //@Input('list-widget') listWidget:string; // Json Object
  listWidgetArray:[any];

  widgetComponents = [];

  constructor(public _widgetLoader:WidgetLoader, public _loader:DynamicComponentLoader, public _elementRef:ElementRef) {

  }

  ngOnInit() {
    //this.listWidgetArray = JSON.parse(this.listWidget);
    //this.render();
  }

  ngOnChanges(changes) {
    this.listWidgetArray = JSON.parse(changes['listWidget'].currentValue);

    this.destroy();
    this.render();
  }

  render() {
    for(let i = 0; i < this.listWidgetArray.length; i++) {
      this._widgetLoader.loadComponent(this.listWidgetArray[i]).then(component => {
        this._loader.loadIntoLocation(component, this._elementRef, 'content').then(widgetComponent=> {
          this.widgetComponents.push(widgetComponent);
        })
      });
    }
  }

  destroy() {
    for(let i = 0; i < this.widgetComponents.length; i++) {
      this.widgetComponents[i].dispose();
    }
  }
}

