// Angular2 specified stuff
import {Component, Output, EventEmitter} from 'angular2/core';

// 3rd party libraries

// Component specified stuff
import {Widget} from './widget';

@Component({
  selector: 'my-widget-component',
  templateUrl: 'src/components/widgets/widget.component.html',
  directives: [
    Widget
  ],
})

// Component setup
export class WidgetComponent {
  listWidget:[any] = [
    {"component": "TestWidgetComponent", "path": "src/components/widgets/components/test.widget.component"},
    {"component": "Test2WidgetComponent", "path": "src/components/widgets/components/test2.widget.component"}
  ];
  listWidgetString:string;

  constructor() {
    this.listWidgetString = JSON.stringify(this.listWidget);
  }

  splice() {
    this.listWidget.splice(0, 1);
    this.listWidgetString = JSON.stringify(this.listWidget);
  }

  add1() {
    this.listWidget.push({"component": "TestWidgetComponent", "path": "src/components/widgets/components/test.widget.component"});
    this.listWidgetString = JSON.stringify(this.listWidget);
  }

  add2() {
    this.listWidget.push({"component": "Test2WidgetComponent", "path": "src/components/widgets/components/test2.widget.component"});
    this.listWidgetString = JSON.stringify(this.listWidget);
  }
}
