// Angular2 specified stuff
import {Component, OnInit} from 'angular2/core';


@Component({
  selector: 'test-widget',
  template: `<div style="height: 100px; text-align: center; font-size: 20px; background: blue;color: white">TestWidget 2</div>`
})

export class Test2WidgetComponent implements OnInit {

  constructor() {

  }

  ngOnInit() {
    console.log('TestWidget');
  }
}

