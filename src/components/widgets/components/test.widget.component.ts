// Angular2 specified stuff
import {Component, OnInit} from 'angular2/core';


@Component({
  selector: 'test-widget',
  template: `<div style="height: 100px; text-align: center; font-size: 20px; background: red;color: white">TestWidget 1</div>`
})

export class TestWidgetComponent implements OnInit {

  constructor() {

  }

  ngOnInit() {
    console.log('TestWidget');
  }
}

