// Angular2 specified stuff
import {Component, ViewEncapsulation} from 'angular2/core';
import {RouteConfig, ROUTER_DIRECTIVES, CanActivate} from 'angular2/router';

// 3rd party libraries

// Component specified stuff
import {Routes} from './routes';

// Component setup
@Component({
  selector: 'my-examples',
  templateUrl: 'src/components/examples/examples.html',
  encapsulation: ViewEncapsulation.None,
  directives: [ROUTER_DIRECTIVES]
})

// Specify component routes
@RouteConfig(Routes.get())

// Actual component class
export class ExamplesCmp {
}
