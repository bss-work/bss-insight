// Route components
import {AuthorCmp} from './author/author';
import {BookCmp} from './book/book';

export class Routes {
  static items = [
    {path: '/author', name: 'Author', component: AuthorCmp},
    {path: '/book', name: 'Book', component: BookCmp}
  ];

  static get():any[] {
    return Routes.items;
  }
}
