// Angular2 specified stuff
import {Component} from 'angular2/core';

// RxJS stuff
import 'rxjs/add/operator/toPromise';

// Component setup
@Component({
  selector: 'book',
  templateUrl: 'src/components/examples/book/book.html'
})

// Actual component class
export class BookCmp {
}
