import {Component} from 'angular2/core';

// Component setup
@Component({
  selector: 'my-footer',
  templateUrl: 'src/components/layout/footer/footer.component.html'
})

// Actual component class
export class FooterComponent {
}
