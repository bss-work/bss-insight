// Angular2 specified stuff
import {Component} from 'angular2/core';
import {CORE_DIRECTIVES, FORM_DIRECTIVES} from 'angular2/common';
import {Location, Router} from 'angular2/router';

// 3rd party libraries
import { ACCORDION_DIRECTIVES } from 'ng2-bootstrap/ng2-bootstrap';

// Component setup
@Component({
  selector: 'my-left-sidebar',
  templateUrl: 'src/components/layout/left-sidebar/left-sidebar.component.html',
  styleUrls: ['src/components/layout/left-sidebar/left-sidebar.component.css'],
  directives: [ACCORDION_DIRECTIVES, CORE_DIRECTIVES, FORM_DIRECTIVES]
})

// Actual component class
export class LeftSidebarComponent {
  constructor(private _location:Location,
              private _router:Router) {
  }

  public oneAtATime:boolean = true;

  public groups:Array<any> = [
    {
      title: 'Menu 1',
      content: 'Menu 1-1'
    },
    {
      title: 'Menu 2',
      content: 'Menu 2-2'
    }
  ];
}
