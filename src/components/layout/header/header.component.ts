// Angular2 specified stuff
import {Component} from 'angular2/core';
import {CORE_DIRECTIVES,} from 'angular2/common';
import {Location, Router} from 'angular2/router';
import {ROUTER_DIRECTIVES} from 'angular2/router';

// 3rd party libraries
import {DROPDOWN_DIRECTIVES} from 'ng2-bootstrap/ng2-bootstrap';

// Component specified stuff
import {Security} from '../../../common/security/security';

// Component setup
@Component({
  selector: 'my-header',
  templateUrl: 'src/components/layout/header/header.component.html',
  styleUrls: ['src/components/layout/header/header.component.css'],
  directives: [
    DROPDOWN_DIRECTIVES,
    CORE_DIRECTIVES,
    ROUTER_DIRECTIVES
  ]
})

// Actual component class
export class HeaderComponent {
  currentUser;

  constructor(private _location:Location,
              private _router:Router,
              private _security:Security) {
    this.currentUser = this._security._currentUser;
  }

  private items:Array<string> = ['The first choice!', 'And another choice for you.', 'but wait! A third!'];

  private toggled(open:boolean):void {
    console.log('Dropdown is now: ', open);
  }

  /**
   * Function to check if specified path is active or not.
   *
   * @param   {string}  path
   * @returns {boolean}
   */
  isActive(path:string):boolean {
    let regExp = new RegExp(path, 'gi');

    return !!this._location.path().match(regExp);
  }

  isRouteActive(routeName:string):boolean {

    return this._router.isRouteActive(this._router.generate([routeName]));
  }

  /**
   * Function to check if current URL is "root" or not.
   *
   * @returns {boolean}
   */
  isRoot():boolean {
    return this._location.path().length === 0;
  }

  /**
   * Function to check if current user is authorized or not.
   *
   * @returns {boolean}
   */
  authorized():boolean {
    return this._security.isAuthenticated();
  }

  /**
   * Function to 'logout' current user. This will just basically remove all items from local storage, where user and
   * actual JWT data exists.
   *
   * @todo add some kind of spinner.
   *
   * @param {Event} event
   */
  logout(event) {
    event.preventDefault();

    this._security.logout();
  }
}
