// Angular2 specified stuff
import {Component,OnInit} from 'angular2/core';
import {FORM_DIRECTIVES, NgForm} from 'angular2/common';
import {Router} from 'angular2/router';

// 3rd party libraries
import {ToastsManager} from 'ng2-toastr/ng2-toastr';

// Component specified stuff
import {App} from '../../app/app';
import {Security} from '../../common/security/security';

// Component setup
@Component({
  selector: 'my-login',
  templateUrl: 'src/components/login/login.component.html',
  styleUrls: ['src/components/login/login.component.css'],
  directives: [FORM_DIRECTIVES],
  providers: [
    Security,
    ToastsManager
  ]
})

export class LoginComponent extends App implements OnInit {
  constructor(public _router:Router, public _security:Security, public _toastr:ToastsManager) {
    super(_router, _security, _toastr);

    if(this._security.isAuthenticated()) {
      this._router.navigate(['/Dashboard']);
    }
  }

  ngOnInit() {
    console.log('LoginComponent');
  }

  submitted = false;
  model = {username: 'huydang1920@gmail.com', password: '123456'};

  onSubmit(form:NgForm) {
    this.submitted = true;
    if(form.valid) {
      this._security.login(this.model).then(
        response => {
          console.log(response);
          this.submitted = false;
          this.showSuccess();
          this.goToMain();
        },
        error=> {
          console.log(error);
        }
      );
    }
  }
}
