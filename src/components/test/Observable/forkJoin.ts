// Angular2 specified stuff
import {Component, OnInit} from 'angular2/core';
import {Router} from 'angular2/router';
import {Observable}  from 'rxjs/Observable';

@Component({
  selector: 'my-fork-join',
  template: 'ForkJoinComponent',
})

// Component setup
export class ForkJoinComponent implements OnInit {
  constructor(public router:Router) {

  }

  ngOnInit():void {
    //Observable.forkJoin(
    //  this._apiService.getColorList(),
    //  this._apiService.getUserList()
    //).subscribe(
    //  res => {
    //    console.log(res[0]);
    //    console.log(res[1]);
    //  },
    //  err => console.error(err)
    //);
  }
}