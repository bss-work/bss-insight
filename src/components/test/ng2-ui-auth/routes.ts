// Route components
import {Ng2UIAuthLogin} from './login/login';
import {Ng2UIAuthSignup} from './signup/signup';

export class Routes {
  static items = [
    {path: '/login', name: 'Ng2UIAuthLogin', component: Ng2UIAuthLogin},
    {path: '/signup', name: 'Ng2UIAuthSignup', component: Ng2UIAuthSignup}
  ];

  static get():any[] {
    return Routes.items;
  }
}
