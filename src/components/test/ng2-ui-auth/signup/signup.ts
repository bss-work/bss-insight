// Angular2 specified stuff
import {Component, ElementRef, Renderer, AfterContentInit, OnInit} from 'angular2/core';
import {FORM_DIRECTIVES, FormBuilder, ControlGroup, Control, Validators} from 'angular2/common';
import {Router, ROUTER_DIRECTIVES} from 'angular2/router';

// 3rd party libraries
import {Auth} from 'ng2-ui-auth';

// Component specified stuff
import {NgMessages, EmailValidator, PasswordMatchValidator} from '../../../../common/directives/directives';

@Component({
  selector: 'my-ng2-ui-auth-signup',
  templateUrl: 'src/components/test/ng2-ui-auth/signup/signup.html'
})

export class Ng2UIAuthSignup implements AfterContentInit, OnInit {
  user:any = {};
  //form:ControlGroup;

  constructor(private auth:Auth,
              private router:Router
              //private element:ElementRef,
              //private renderer:Renderer,
              //private fb:FormBuilder
  ) {

  }

  ngAfterContentInit():any {
    //this.renderer.setElementClass(this.element.nativeElement, 'app', true);
  }

  ngOnInit() {
    //this.form = this.fb.group({
    //  displayName: new Control('', Validators.compose([Validators.required, Validators.maxLength(32)])),
    //  email: new Control('', Validators.compose([Validators.required, EmailValidator.validate])),
    //  passwordGroup: new ControlGroup(
    //    {
    //      password: new Control('', Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(32)])),
    //      confirmPassword: new Control('')
    //    },
    //    null,
    //    PasswordMatchValidator.validate
    //  )
    //});
  }

  signup() {
    //this.auth.signup({email: this.user.email, password: this.user.password})
    //  .subscribe(() => {
    //    this.auth.login({email: this.user.email, password: this.user.password})
    //      .subscribe(() => {
    //        if(this.auth.isAuthenticated()) {
    //          this.router.navigate(['Dashboard']);
    //        }
    //      });
    //  });
  }
}
