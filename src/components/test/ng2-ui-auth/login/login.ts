// Angular2 specified stuff
import {Component, AfterContentInit, ElementRef, Renderer, OnInit} from 'angular2/core';
import {FormBuilder, ControlGroup, Control, Validators} from 'angular2/common';
import {Router, ROUTER_DIRECTIVES} from 'angular2/router';

// 3rd party libraries
import {ToastsManager} from 'ng2-toastr/ng2-toastr';

// Component specified stuff
import {App} from '../../../../app/app';
import {NgMessages, EmailValidator} from '../../../../common/directives/directives';
import {Security} from '../../../../common/security/security';

// Actual component class
@Component({
  selector: 'my-ng2-ui-auth-login',
  templateUrl: 'src/components/test/ng2-ui-auth/login/login.html',
  directives: [NgMessages, ROUTER_DIRECTIVES, EmailValidator],
  providers: [
    ToastsManager
  ]
})

export class Ng2UIAuthLogin extends App implements OnInit, AfterContentInit {
  user = {email: '', password: ''};
  //form:ControlGroup;
  response:any;

  constructor(public _router:Router, public _security:Security, public _toastr:ToastsManager) {
    super(_router, _security, _toastr);
  }

  //constructor(public _router:Router,
  //            public _security:Security,
  //            //private element:ElementRef,
  //            //private renderer:Renderer,
  //            //private fb:FormBuilder,
  //            public _toastr:ToastsManager) {
  //  super(_router, _security, _toastr);
  //}

  ngAfterContentInit() {
    console.log('ngAfterContentInit');
    //this.renderer.setElementClass(this.element.nativeElement, 'app', true);
    //if(this.auth.isAuthenticated()) {
    //  this.goToMain();
    //}
  }

  ngOnInit() {
    console.log('ngOnInit');
    //this.form = this.fb.group({
    //  email: new Control('', Validators.compose([Validators.required, EmailValidator.validate])),
    //  password: new Control('', Validators.required)
    //});
  }

  login() {
    //this.auth.login(this.user)
    //  .subscribe(() => this.goToMain());
  }

  authenticate(provider:string) {
    //this.auth.authenticate(provider)
    //  .subscribe(() => this.goToMain());
    this._security.authenticate(provider).then(
      response => {
        console.log(response);
        //this.goToMain();
        this.showSuccess();
        this.response = response;
      },
      error=> {
        console.log(error);
      }
    );
  }
}
