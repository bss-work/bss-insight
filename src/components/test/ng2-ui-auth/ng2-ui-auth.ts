// Angular2 specified stuff
import {Component, ViewEncapsulation} from 'angular2/core';
import {RouteConfig, ROUTER_DIRECTIVES, CanActivate} from 'angular2/router';
import {Http} from 'angular2/http';

// 3rd party libraries
import {Auth, JwtHttp, Config} from 'ng2-ui-auth';
import {Observable} from 'rxjs/Observable';

// Component specified stuff
import {Routes} from './routes';

// Component setup
@Component({
  selector: 'my-ng2-ui-auth',
  template: `<router-outlet></router-outlet>`,
  encapsulation: ViewEncapsulation.None,
  directives: [ROUTER_DIRECTIVES]
})

// Specify component routes
@RouteConfig(Routes.get())

// Actual component class
export class Ng2UIAuthComponent {
  //helloWorld:Observable<string>;
  //helloWorldError:string = 'helloWorld';
  //
  //constructor(private auth:Auth,
  //            private router:Router,
  //            private jwtHttp:JwtHttp,
  //            private http:Http) {
  //  this.helloWorld = jwtHttp.get('/api/helloWorld').map((response) => response.text());
  //  http.get('/api/helloWorld')
  //    .subscribe(
  //      (response) => {
  //        this.helloWorldError = response.text();
  //      },
  //      (response) => {
  //        this.helloWorldError = response.text();
  //      });
  //}
  //
  //signout() {
  //  this.auth.logout().subscribe(() => {
  //    this.router.navigate(['/Login']);
  //  })
  //}
}
