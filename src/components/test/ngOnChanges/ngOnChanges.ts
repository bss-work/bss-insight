// Angular2 specified stuff
import {Component, Input, OnChanges, OnInit, SimpleChange} from 'angular2/core';

@Component({
  selector: 'my-cmp',
  template: `
  <p>myProp = {{myProp}}</p>
  <p>myArray = {{myArray | json}}</p>
  <p>myString = {{myString}}</p>
  `
})
class TestChangeComponent implements OnChanges, OnInit {
  @Input('my-prop') myProp:any;
  @Input('my-array') myArray:Array<any>;
  @Input('my-string') myString:string;

  ngOnInit() {

  }

  ngOnChanges(changes) {
    if(changes['myProp']) {
      console.log('ngOnChanges - myProp = ' + changes['myProp'].currentValue);
    }
    if(changes['myArray']) {
      console.log('ngOnChanges - myArray = ', changes['myArray'].currentValue);
    }
    if(changes['myString']) {
      console.log('ngOnChanges - myString = ' + changes['myString'].currentValue);
    }
  }
}

@Component({
  selector: 'my-onchange',
  template: `
    <button (click)="value = value + 1">Change MyComponent</button>
    <button (click)="add(1)">Change Array</button>
    <my-cmp [my-prop]="value" [my-array]="data" [my-string]="dataString"></my-cmp>`,
  directives: [TestChangeComponent]
})
export class OnChangeComponent {
  value = 0;
  data = [1, 2, 3, 4];
  dataString:string;

  constructor() {
    this.dataString = JSON.stringify(this.data);
  }

  add(value:any) {
    this.data.push(value);
    this.dataString = JSON.stringify(this.data);
  }
}