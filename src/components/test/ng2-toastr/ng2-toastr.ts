// Angular2 specified stuff
import {Component} from 'angular2/core';

// 3rd party libraries
import {ToastsManager} from 'ng2-toastr/ng2-toastr';

@Component({
  selector: 'my-toastr',
  template: '<button class="btn btn-default" (click)="showSuccess()">Toastr Tester</button>',
  providers: [ToastsManager]
})

// Component setup
export class ToastrComponent {

  constructor(public _toastr:ToastsManager) {
  }

  showSuccess() {
    this._toastr.success('You are awesome!', 'Success!');
  }

  showError() {
    this._toastr.error('This is not good!', 'Oops!');
  }

  showWarning() {
    this._toastr.warning('You are being warned.', 'Alert!');
  }

  showInfo() {
    this._toastr.info('Just some information for you.');
  }
}