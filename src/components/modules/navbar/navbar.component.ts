import {Component} from 'angular2/core';
import {ROUTER_DIRECTIVES} from 'angular2/router';

@Component({
  selector: 'my-navbar',
  templateUrl: 'src/components/modules/navbar/navbar.component.html',
  styleUrls: ['src/components/modules/navbar/navbar.component.css'],
  directives: [ROUTER_DIRECTIVES]
})
export class NavbarComponent {
}
