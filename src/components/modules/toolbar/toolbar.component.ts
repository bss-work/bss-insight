import {Component} from 'angular2/core';

@Component({
  selector: 'my-toolbar',
  templateUrl: 'src/components/modules/toolbar/toolbar.component.html',
  styleUrls: ['src/components/modules/toolbar/toolbar.component.css']
})
export class ToolbarComponent {
}
