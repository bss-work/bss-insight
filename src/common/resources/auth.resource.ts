// Angular2 specified stuff
import {Injectable} from 'angular2/core';
import {Http, Response, Headers, RequestOptions} from 'angular2/http';

// RxJS stuff
import 'rxjs/add/operator/map';

// 3rd party libraries
import {Observable} from 'rxjs/Observable';

// Component specified stuff
import {CONFIG} from './../../config';
import {ApiService} from './../services/api.service';

@Injectable()
export class AuthResource extends ApiService {
  constructor(_http:Http, private _apiService:ApiService) {
    super(_http);
  }

  // Check login
  auth() {
    let headers = ApiService.getHeaders();
    let options = new RequestOptions({headers: headers});

    return this._http
      .get(CONFIG.api.checkAuth, options)
      .toPromise()
      .then((response) => response.json())
      .catch(this.handleError);
  }

  // Login by username or email and password
  login(params) {
    let body = JSON.stringify(params);
    let headers = ApiService.getHeaders();
    let options = new RequestOptions({headers: headers});

    return this._http
      .post(CONFIG.api.login, body, options)
      .toPromise()
      .then((response) => response.json())
      .catch(this.handleError);
  }

  private handleError(error:any) {
    // in a real world app, we may send the error to some remote logging infrastructure
    // instead of just logging it to the console
    console.log('status:' + error.status);
    return Promise.reject('Server error');
  }

  //post2(params) {
  //  let body = JSON.stringify({params});
  //  let headers = new Headers({'Content-Type': 'application/json'});
  //  let options = new RequestOptions({headers: headers});
  //
  //  return this._http
  //    .post(this._apiUrl, body, options)
  //    .subscribe(
  //      response => {
  //        let data = response.json();
  //        console.log(data);
  //      },
  //      error => {
  //        console.log('error');
  //        console.log(error);
  //      }
  //    );
  //}
}
