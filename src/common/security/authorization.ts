// Angular2 specified stuff
import {Injectable} from 'angular2/core';
import {Router} from 'angular2/router';

// 3rd party libraries

// Component specified stuff
import {CONFIG} from './../../config';
import {AuthResource} from './../resources/resources';
import {ApiService} from './../services/services';
import {Auth} from 'ng2-ui-auth';

@Injectable()
export class Security {
  _currentUser;
  _authToken;

  constructor(private _authResource:AuthResource, private _auth:Auth, private _router:Router) {
  }

  login(params) {
    return new Promise((resolve, reject) =>
      this._authResource.login(params).then(
        response => {
          this._currentUser = response.data;
          this._authToken = response.authToken;

          localStorage.setItem('user', JSON.stringify(this._currentUser));
          localStorage.setItem(CONFIG.security.tokenName, this._authToken);

          // Set the Request Header 'Authorization'
          ApiService.setAuthTokenHeader(this._authToken);

          resolve(this._currentUser);
        },
        error=> {
          reject(error);
        })
    );
  }

  // Login by ng2-ui-auth
  authenticate(provider:string) {
    return new Promise((resolve, reject) =>
      this._auth.authenticate(provider)
        .subscribe(
          response => {
            response = response.json();

            this._currentUser = response.data;
            this._authToken = response.authToken;

            localStorage.setItem('user', JSON.stringify(this._currentUser));
            localStorage.setItem(CONFIG.security.tokenName, this._authToken);

            // Set the Request Header 'Authorization'
            ApiService.setAuthTokenHeader(this._authToken);

            resolve(this._currentUser);
          },
          error=> {
            reject(error);
          }
        )
    );
  }

  logout() {
    // Remove localStorage
    //localStorage.removeItem('user');
    //localStorage.removeItem(CONFIG.security.tokenName);
    localStorage.clear();
    // Remove class
    this._currentUser = null;
    this._authToken = null;

    this._router.navigate(['Login']).then(() => {
      console.log('logged out successfully');
    });
  }

  // Is the current user authenticated?
  isAuthenticated() {
    return !!this._currentUser;
  }

  // Ask the backend to see if a user is already authenticated - this may be from a previous session.
  requestCurrentUser() {
    return new Promise((resolve, reject) => {
        let authToken = localStorage.getItem(CONFIG.security.tokenName);
        if(authToken) {
          // Set the Request Header 'Authorization'
          ApiService.setAuthTokenHeader(authToken);

          if(!this.isAuthenticated()) {
            this._authResource.auth().then(
              response => {
                console.log(response);

                this._currentUser = response.data;
                this._authToken = response.authToken;

                localStorage.setItem('user', JSON.stringify(this._currentUser));
                localStorage.setItem(CONFIG.security.tokenName, this._authToken);

                resolve(this._currentUser);
              },
              error=> {
                reject(false);
              })
          } else {
            resolve(this._currentUser);
          }
        } else {
          reject(false);
        }
      }
    );
  }


  // Get auth token
  getToken() {
    return !!(this._authToken || localStorage.getItem(CONFIG.security.tokenName));
  }
}
