// Angular2 specified stuff
import {Injectable} from 'angular2/core';
import {Http, Response, Headers, RequestOptions} from 'angular2/http';

// RxJS stuff
import 'rxjs/add/operator/map';

// 3rd party libraries

@Injectable()
export class ApiService {
  _authToken;

  /**
   * Construction of the class.
   */
  //constructor(protected _http:Http, protected _authHttp:AuthHttp) {}
  constructor(protected _http:Http) {
  }

  public static setAuthTokenHeader(authToken) {
    this._authToken = authToken;
  }

  // TODO: move to common service
  public static getHeaders():Headers {
    let headers = new Headers();

    headers.append('Content-Type', 'application/json');

    if(this._authToken) {
      headers.append('Authorization', this._authToken);
    }

    return headers;
  }

  // TODO: move to common service
  public static parseParameters(parameters:Object):string {
    if(Object.keys(parameters).length === 0) {
      return '';
    } else {
      return '?' + ApiService.serialize(parameters);
    }
  }

  // TODO: move to common service
  private static serialize(obj, prefix?) {
    let str = [];

    for(let p in obj) {

      if(obj.hasOwnProperty(p)) {
        let k = prefix ? prefix + '[' + p + ']' : p, v = obj[p];

        str.push(typeof v === 'object' ? ApiService.serialize(v, k) : encodeURIComponent(k) + '=' + encodeURIComponent(v));
      }
    }

    return str.join('&');
  }

  ///**
  // * Getter method for book count
  // *
  // * @param params
  // * @returns {Observable<R>}
  // */
  //count({params = {}}: {params?: Object} = {}) {
  //  return this._authHttp
  //    .get(this.apiUrl + '/book/count' + BookService.parseParameters(params), {headers: BookService.getHeaders()})
  //    .map(res => res.json());
  //}
  //
  ///**
  // * Getter method for books data.
  // *
  // * @param params
  // * @returns {Observable<R>}
  // */
  //getBooks(params?: Object) {
  //  return this._authHttp
  //    .get(this.apiUrl + '/book' + BookService.parseParameters(params), {headers: BookService.getHeaders()})
  //    .map(res => res.json());
  //}
}
