export const CONSTANTS = {
  APP_BASE: 'http://localhost/bss-insight',

  GOOGLE_API_KEY: '',
  GOOGLE_CLIENT_ID: '732070508147-tjjj89j114819aste3c54qjo946g4mob.apps.googleusercontent.com',
  GOOGLE_REDIRECT_URI: 'http://localhost/bss-insight/oauth2callback.html',

  FACEBOOK_APP_ID: '1015542651820710',
  FACEBOOK_APP_VERSION: 'v2.5',
  FACEBOOK_SCOPE: ['email', 'user_likes', 'user_photos', 'user_birthday', 'public_profile'],
  FACEBOOK_AUTHORIZATION_END_POINT: 'https://www.facebook.com/v2.5/dialog/oauth',
  FACEBOOK_REDIRECT_URI: 'http://localhost/bss-insight/oauthcallback.html'
};
