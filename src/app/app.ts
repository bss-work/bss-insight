// Angular2 specified stuff
import {Component, Injectable} from 'angular2/core';
import {Router} from 'angular2/router';

// 3rd party libraries
import {ToastsManager} from 'ng2-toastr/ng2-toastr';

// Component specified stuff
import {Security} from '../common/security/security';

@Injectable()
export class App {
  _isAuthenticated:boolean = false;

  constructor(public _router:Router, public _security:Security, public _toastr:ToastsManager) {
  }

  showSuccess() {
    this._toastr.success('You are awesome!', 'Success!');
  }

  showError() {
    this._toastr.error('This is not good!', 'Oops!');
  }

  showWarning() {
    this._toastr.warning('You are being warned.', 'Alert!');
  }

  showInfo() {
    this._toastr.info('Just some information for you.');
  }

  redirectLogin() {
    if(!this._isAuthenticated) {
      this._router.navigate(['Login']);
    }
  }

  goToMain() {
    this._router.navigate(['Dashboard']);
  }
}
