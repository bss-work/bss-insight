// Angular2 specified stuff
import {Directive, Attribute, ElementRef, DynamicComponentLoader} from 'angular2/core';
import {Router, RouterOutlet, ComponentInstruction} from 'angular2/router';

// Component specified stuff
import {Security} from '../common/security/security';

@Directive({
  selector: 'my-router-outlet'
})
export class MyRouterOutlet extends RouterOutlet {
  _publicRoutes:any;
  _router:Router;

  constructor(_elementRef:ElementRef, _loader:DynamicComponentLoader,
              _parentRouter:Router, @Attribute('name') nameAttr:string,
              private _security:Security) {
    super(_elementRef, _loader, _parentRouter, nameAttr);

    this._router = _parentRouter;
    this._publicRoutes = {
      '#/login': true
    };
  }

  activate(instruction:ComponentInstruction) {
    //var url = this.parentRouter.lastNavigationAttempt;
    var url = location.hash;
    if(!this._publicRoutes[url] && !this._security.getToken()) {
      // todo: redirect to Login, may be there a better way?
      this._router.navigateByUrl('/login');
    }
    return super.activate(instruction);
  }
}
