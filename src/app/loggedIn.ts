// Angular2 specified stuff
import {Component, Injectable} from 'angular2/core';
import {Router} from 'angular2/router';

// Component specified stuff
import {App} from './app';
import {Security} from '../common/security/security';

@Injectable()
export class LoggedIn extends App {
  constructor(public _router:Router, public _security:Security) {
    super(_router, _security);

    _security.requestCurrentUser().then(
      response=> {
        this._isAuthenticated = _security.isAuthenticated();
        this.redirectLogin();
      },
      error=> {
        this.redirectLogin();
      }
    );
  }
}
