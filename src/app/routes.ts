import {DashboardComponent} from '../components/dashboard/dashboard.component';
import {LoginComponent} from '../components/login/login.component';
import {AboutComponent} from '../components/about/about.component';
import {ExamplesCmp} from '../components/examples/examples';
import {ToastrComponent} from '../components/test/ng2-toastr/ng2-toastr';
import {Ng2UIAuthComponent} from '../components/test/ng2-ui-auth/ng2-ui-auth';
import {WidgetComponent} from '../components/widgets/widget.component';

import {OnChangeComponent} from '../components/test/ngOnChanges/ngOnChanges';

export class Routes {
  static items = [
    {path: '/dashboard', name: 'Dashboard', component: DashboardComponent, useAsDefault: true},
    {path: '/login', name: 'Login', component: LoginComponent},
    {path: '/about', name: 'About', component: AboutComponent},
    {path: '/examples/...', name: 'Examples', component: ExamplesCmp},
    {path: '/test-toastr', name: 'Toastr', component: ToastrComponent},
    {path: '/test-ng2-ui-auth/...', name: 'Ng2UIAuth', component: Ng2UIAuthComponent},
    {path: '/test-widget', name: 'WidgetTest', component: WidgetComponent},
    {path: '/ng-on-change', name: 'NgOnChange', component: OnChangeComponent}
  ];

  static get():any[] {
    return Routes.items;
  }
}
