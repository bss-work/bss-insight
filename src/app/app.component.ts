// Angular2 specified stuff
import {Component, OnInit} from 'angular2/core';
import {Router, RouteConfig, ROUTER_DIRECTIVES} from 'angular2/router';

// 3rd party libraries
import 'rxjs/add/operator/toPromise';
import {Auth} from 'ng2-ui-auth';

// Component specified stuff
import {App} from './app';
import {MyRouterOutlet} from './router-outlet';
import {Routes} from './routes';

import {CONFIG} from '../config';
import {CONSTANTS} from '../constants';

import {HeaderComponent, FooterComponent} from '../components/layout/layout';
import {LeftSidebarComponent, RightSidebarComponent} from '../components/layout/layout';
import {SpinnerComponent} from '../components/modules/spinner/spinner.component';

import {ApiService} from '../common/services/services';
import {AuthResource} from '../common/resources/resources';
import {Security} from '../common/security/security';

// Component setup
@Component({
  selector: 'my-app',
  templateUrl: 'src/app/app.component.html',
  directives: [
    ROUTER_DIRECTIVES,
    MyRouterOutlet,
    HeaderComponent,
    FooterComponent,
    LeftSidebarComponent,
    RightSidebarComponent,
    SpinnerComponent
  ],
  providers: [
    Auth,
    ApiService,
    AuthResource,
    Security
  ]
})

// Specify component routes
@RouteConfig(Routes.get())

// Actual component class
export class AppComponent extends App implements OnInit {
  constructor(public _router:Router, public _security:Security) {
    super(_router, _security);

    this._isAuthenticated = _security.isAuthenticated();
    _security.requestCurrentUser().then(
      response=> {
        this._isAuthenticated = _security.isAuthenticated();
      }
    );
  }

  ngOnInit() {
    console.log('AppComponent');
  }
}
