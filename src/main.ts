// Angular2 specified stuff
import {bootstrap} from 'angular2/platform/browser';
import {enableProdMode, provide} from 'angular2/core';
import {APP_BASE_HREF, LocationStrategy, HashLocationStrategy} from 'angular2/router';
import {ROUTER_PROVIDERS} from 'angular2/router';
import {HTTP_PROVIDERS} from 'angular2/http';
import {NG2_UI_AUTH_PROVIDERS} from 'ng2-ui-auth';
import {ToastOptions} from "ng2-toastr/ng2-toastr";

// Component specified stuff
import {AppComponent} from './app/app.component';
import {CONFIG} from './config';
import {CONSTANTS} from './constants';

enableProdMode();

bootstrap(AppComponent, [
  HTTP_PROVIDERS,
  ROUTER_PROVIDERS,
  //provide(APP_BASE_HREF, {useValue: '#'}),
  provide(LocationStrategy, {useClass: HashLocationStrategy}),
  NG2_UI_AUTH_PROVIDERS({
    tokenName: 'authToken',
    tokenPrefix: false,
    providers: {
      google: {
        clientId: CONSTANTS.GOOGLE_CLIENT_ID,
        redirectUri: CONSTANTS.GOOGLE_REDIRECT_URI,
        url: CONFIG.api.googleLogin
      },
      facebook: {
        clientId: CONSTANTS.FACEBOOK_APP_ID,
        authorizationEndpoint: CONSTANTS.FACEBOOK_AUTHORIZATION_END_POINT,
        redirectUri: CONSTANTS.FACEBOOK_REDIRECT_URI,
        scope: CONSTANTS.FACEBOOK_SCOPE,
        type: CONSTANTS.FACEBOOK_APP_VERSION.replace(/[^0-9.]+/g, ''),
        url: CONFIG.api.facebookLogin
      }
    }
  }),
  provide(ToastOptions, {
    useValue: new ToastOptions({
      autoDismiss: true,
      positionClass: 'toast-top-right',
    })
  }),
]);
