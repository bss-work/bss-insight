export const CONFIG = {
  api: {
    checkAuth: 'http://localhost/bss-insight-api/public/auth',
    login: 'http://localhost/bss-insight-api/public/auth/login',
    facebookLogin: 'http://localhost/bss-insight-api/public/auth/facebook',
    googleLogin: 'http://localhost/bss-insight-api/public/auth/google',
    logout: 'http://localhost/bss-insight-api/public/auth/logout'
  },
  security: {
    tokenName: 'authToken'
  }
};
